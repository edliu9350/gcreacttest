# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# README #

Develop a Single Page App (SPA) that shows a simple dashboard that displays four tiles, each displaying weather information

* Temperature (Minimum / Maximum)
* Wind (Speed and direction)
* Humidity
* Pressure

The data will be retrieved via the following REST API. (https://openweathermap.org/api). The user should be able to request new data to be loaded and be able to change the 
country. The list of the countries to be retrieved from the following rest api (https://restcountries.com/v3.1/all)

Statistics in tiles will update to reflect the new dataset. 

### Requirements ###

* The app shall be implemented using ReactJS components and developed using TypeScript.
* The app shall display four tiles with information about tempreture, Wind, Humidiy and Pressure.
* The app shall have a drop down with countries. When selection is changed the new dataset will be loaded (replacing the current dataset) and the dashboard display will be update to reflect the newly loaded data.

### Structure ###
* "hooks/useForecasts": Hook for fetching weather data from OpenweatherMap API
* "components": Reusable weather components
* "views/ForecastPage": Main page