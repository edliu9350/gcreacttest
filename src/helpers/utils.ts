/**
 * @description global helper functions
 */
import moment from "moment";

/**
 * @param date Date
 * @description convert date to weekday (ex. 04/02/2022->Saturday)
 */
const getWeekdayString = (date: Date): string => {
    return moment(date).format("dddd");
};

/**
 * @param date Date
 * @description convert date to day string (ex. 04/02/2022->April 2nd)
 */
const getDayString = (date: Date): string => {
    return moment(date).format("MMM Do");
};

/**
 * @param id ID of weather icon
 * @description get url of weather icon
 */
const getWeatherIconURL = (id: string): string => {
    return `http://openweathermap.org/img/wn/${id}@2x.png`;
};

/**
 * @param deg angle refering to direction
 * @description get direction name
 */
const getDirectionString = (deg: number): string => {
    let val: number = Math.floor(deg / 22.5 + 0.5);
    let arr: Array<string> = [
        "N",
        "NNE",
        "NE",
        "ENE",
        "E",
        "ESE",
        "SE",
        "SSE",
        "S",
        "SSW",
        "SW",
        "WSW",
        "W",
        "WNW",
        "NW",
        "NNW",
    ];
    return arr[val % 16];
};

export {
    getWeekdayString,
    getDayString,
    getWeatherIconURL,
    getDirectionString,
};
