/**
 * @description Temperature panel
 */

import {
    getDayString,
    getWeekdayString,
    getWeatherIconURL,
} from "../../helpers/utils";
import { Weather } from "../../interface";
import styles from "./TemperaturePanel.module.scss";
import UpcomingDaysForecast from "../UpcomingDaysForecast";
import locationIcon from "./assets/location-pin.png";

interface TemperaturePanelProps {
    forecast: Weather | null;
    upcomingForecasts: Array<Weather>;
}

/**
 * @param forecast Weather
 * @param upcomingForecasts Array<Weather>
 */
export default ({ forecast, upcomingForecasts }: TemperaturePanelProps) => {
    const date = new Date();
    return (
        <div>
            <h2 className="font-weight-bold mb-1">TEMPERATURE</h2>
            <h3 className="font-weight-bold mb-1">
                {forecast!.main.temp} °C{" "}
                <img
                    className="mb-2"
                    width="35"
                    src={getWeatherIconURL(forecast!.weather[0].icon)}
                    alt=""
                />
            </h3>
            <h4 className="mb-1">{forecast!.weather[0].description}</h4>
            <p className="mb-0">
                {forecast &&
                    `${getDayString(date)} (${getWeekdayString(date)})`}
            </p>
            <p
                className={`${styles.location} d-flex align-items-baseline font-weight-lighter mb-1`}
            >
                <img
                    width="10"
                    height="15"
                    src={locationIcon}
                    className={styles.mr1}
                    alt="location pin icon"
                />
                <span>{forecast && forecast.name}</span>
            </p>
            <div className="mt-4 mt-md-2">
                <div className="d-flex flex-column mb-2">
                    <div className="d-flex justify-content-around">
                        <p className="mb-0 font-weight-bolder text-uppercase">
                            <span className={styles.label}>
                                Max Temperature
                            </span>
                            {forecast && forecast.main.temp_max} &deg;C
                        </p>
                    </div>
                    <div className="d-flex justify-content-around">
                        <p className="mb-0 font-weight-bolder text-uppercase">
                            <span className={styles.label}>
                                Min Temperature
                            </span>
                            {forecast && forecast.main.temp_min} &deg;C
                        </p>
                    </div>
                </div>
            </div>
            <UpcomingDaysForecast upcomingForecasts={upcomingForecasts} />
        </div>
    );
};
