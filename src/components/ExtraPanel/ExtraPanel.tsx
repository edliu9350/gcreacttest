/**
 * @description Wind, Humidity and Pressure Panel
 */

import styles from "./ExtraPanel.module.scss";
import { Weather } from "../../interface";
import { getDirectionString } from "../../helpers/utils";

interface ExtraPanelProps {
    forecast: Weather | null;
    factor: string;
}

/**
 * @param forecast Weather
 * @param factor string(wind, humidity or pressure)
 */
export default ({ forecast, factor }: ExtraPanelProps) => {
    let value: string = "",
        unit: string = "",
        description: string = "\t";
    let gradientStyle = styles.gradient;
    let imgStyle = styles.img;

    if (forecast) {
        switch (factor) {
            case "wind":
                value = String(forecast.wind.speed);
                unit = "km/h";
                description = "wind speed";
                gradientStyle = styles.gradientWind;
                imgStyle = styles.imgWind;
                break;
            case "humidity":
                value = String(forecast.main.humidity);
                unit = "%";
                description = "humidity";
                gradientStyle = styles.gradientHumidity;
                imgStyle = styles.imgHumidity;
                break;
            case "pressure":
                value = String(forecast.main.pressure);
                unit = "mb";
                description = "air pressure";
                gradientStyle = styles.gradientPressure;
                imgStyle = styles.imgPressure;
                break;
        }
    }

    return (
        <div className="d-flex">
            <div className={imgStyle}></div>
            <div className={gradientStyle}></div>
            <div
                className={`${styles.cardInner} d-flex flex-column justify-content-between pt-3 pb-2 pl-2`}
            >
                <h2 className="font-weight-bold text-center">
                    {factor.toUpperCase()}
                </h2>
                {forecast && (
                    <div>
                        <h2 className="font-weight-bold mb-1">
                            <span>{value}</span>&nbsp;
                            {unit}
                            {factor == "wind" && (
                                <>
                                    <br />
                                    {getDirectionString(forecast.wind.deg)}
                                </>
                            )}
                        </h2>
                        <h5 className="font-weight-lighter">{description}</h5>
                    </div>
                )}
            </div>
        </div>
    );
};
