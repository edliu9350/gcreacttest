/**
 * @description Component for temperature forecast (4 days)
 */

import { getWeatherIconURL, getWeekdayString } from "../../helpers/utils";
import { Weather } from "../../interface";
import styles from "./UpcomingDaysForecast.module.scss";

interface UpcomingDaysForecastProps {
    upcomingForecasts: Array<Weather>;
}

/**
 * @param upcomingForecasts Array<Weather>
 */
export default ({ upcomingForecasts }: UpcomingDaysForecastProps) => {
    return (
        <ul className={`${styles.weekList} d-flex p-0`}>
            {upcomingForecasts.map((forecast: Weather, index: number) => (
                <li
                    className={`${styles.weekday} d-flex flex-column justify-content-center align-items-center p-2`}
                    key={index}
                >
                    <img
                        className="mb-2"
                        width="30"
                        src={getWeatherIconURL(forecast.weather[0].icon)}
                        alt=""
                    />
                    <span className="mb-2">
                        {getWeekdayString(new Date(forecast.dt))}
                    </span>
                    <span className="font-weight-bold">
                        {forecast.main.temp} &deg;C
                    </span>
                </li>
            ))}
        </ul>
    );
};
