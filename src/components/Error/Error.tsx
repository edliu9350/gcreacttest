/**
 * @description Error message component
 */

import styles from "./Error.module.scss";

interface ErrorProps {
    message: string;
}

const Error = ({ message }: ErrorProps) => (
    <div className={`${styles.error} alert position-absolute`} role="alert">
        {message}
    </div>
);

export default Error;
