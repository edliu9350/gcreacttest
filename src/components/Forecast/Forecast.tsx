/**
 * @description Main component for weather
 */

import { Weather, Country } from "../../interface";
import styles from "./Forecast.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import ExtraPanel from "../ExtraPanel";
import TemperaturePanel from "../TemperaturePanel";

interface ForecastProps {
    forecast: Weather;
    upcomingForecasts: Array<Weather>;
    onSubmit(country: Country): void;
}

/**
 * @param forecast Weather
 * @param upcomingForecasts Array<Weather>
 * @param onSubmit function for fetching a country's weather data
 */
export default ({ forecast, upcomingForecasts, onSubmit }: ForecastProps) => {
    return (
        <div>
            <Container className={styles.box}>
                <Row className={styles.container}>
                    <Col
                        xs={12}
                        md={12}
                        className="d-flex flex-column justify-content-between"
                    >
                        <TemperaturePanel
                            forecast={forecast}
                            upcomingForecasts={upcomingForecasts}
                        />
                    </Col>
                </Row>
            </Container>
            <Container className={styles.box2}>
                <Row>
                    <Col xs={12} md={4} className={styles.container}>
                        <div className={styles.card}>
                            <ExtraPanel forecast={forecast} factor="wind" />
                        </div>
                    </Col>
                    <Col xs={12} md={4} className={styles.container}>
                        <div className={styles.card}>
                            <ExtraPanel forecast={forecast} factor="humidity" />
                        </div>
                    </Col>
                    <Col xs={12} md={4} className={styles.container}>
                        <div className={styles.card}>
                            <ExtraPanel forecast={forecast} factor="pressure" />
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};
