/**
 * @description Header
 */

import styles from "./Header.module.scss";

export default () => {
    return <h1 className={styles.heading}>Weather Information</h1>;
};
