/**
 * @description hook for fetching data from API
 */

import { useEffect, useState, useCallback } from "react";
import axios from "axios";
import { Country, Weather } from "../interface";

const COUNTRY_URL = "https://restcountries.com/v3.1/all";
const WEATHER_CURRENT_URL = "https://api.openweathermap.org/data/2.5/weather";
const WEATHER_HOURLY_URL =
    "https://api.openweathermap.org/data/2.5/forecast/hourly";
const WEATHER_DAILY_URL =
    "https://api.openweathermap.org/data/2.5/forecast/daily";
const APP_ID = "ad29264c379628a908cf6b07f1e208ac";

export default () => {
    const [isError, setError] = useState<boolean>(false);
    const [isLoading, setLoading] = useState<boolean>(true);
    const [forecast, setForecast] = useState<Weather | null>(null);
    const [upcomingForecasts, setUpcomingForecasts] = useState<Array<Weather>>(
        []
    );
    const [countryList, setCountryList] = useState<Array<Country>>([]);

    useEffect(() => {
        axios
            .get(COUNTRY_URL)
            .then((res) => {
                //change data form
                res.data.forEach((country: any) => {
                    country.name = country.name.common;
                });

                setCountryList(res.data);
            })
            .catch((err) => {
                setError(true);
            });
    }, []);

    const submitRequest = useCallback(
        async (country: Country) => {
            setLoading(true);
            setError(false);

            try {
                const { data } = await axios(
                    `${WEATHER_CURRENT_URL}?lat=${country.latlng[0]}&lon=${country.latlng[1]}&appid=${APP_ID}`
                );
                // if (!data || data.list.length < 96) {
                //     setError(true);
                //     return;
                // }

                // //convert temp unit
                // data.list.forEach((item: Weather) => {
                //     item.main.temp = Math.floor(item.main.temp - 273);
                //     item.main.temp_max = Math.floor(item.main.temp_max - 273);
                //     item.main.temp_min = Math.floor(item.main.temp_min - 273);
                //     item.dt *= 1000;
                // });

                // setForecast(data.list[0]);
                // setUpcomingForecasts([
                //     data.list[23],
                //     data.list[47],
                //     data.list[71],
                //     data.list[95],
                // ]);

                data.main.temp = Math.floor(data.main.temp - 273);
                data.main.temp_max = Math.floor(data.main.temp_max - 273);
                data.main.temp_min = Math.floor(data.main.temp_min - 273);

                data.dt *= 1000;
                setForecast(data);
                setUpcomingForecasts([data, data, data, data]);

                //get min & max temp
                // const { data: dailyData } = await axios(
                //     `${WEATHER_DAILY_URL}?lat=${country.latlng[0]}&lon=${country.latlng[1]}&appid=${APP_ID}&cnt=1`
                // );
                // data.main.temp_max = Math.floor(
                //     dailyData.list[0].temp.max - 273
                // );
                // data.main.temp_min = Math.floor(
                //     dailyData.list[0].temp.min - 273
                // );

                setLoading(false);
            } catch (err) {
                setError(true);
            }
        },
        [countryList]
    );

    return {
        isError,
        isLoading,
        forecast,
        upcomingForecasts,
        countryList,
        submitRequest,
    };
};
