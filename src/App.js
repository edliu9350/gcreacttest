import logo from "./logo.svg";
import "./App.css";
import ForecastPage from "./views/ForecastPage";

function App() {
    return (
        <div className="App">
            <ForecastPage />
        </div>
    );
}

export default App;
