/**
 * @description Country interface
 */

interface Country {
    name: string;
    region: string;
    subregion: string;
    latlng: number[];
}

export default Country;
