/**
 * @description main page
 */

import { Fragment, useState } from "react";
import useForecast from "../../hooks/useForecast";
import Header from "../../components/Header";
import styles from "./ForecastPage.module.scss";
import Forecast from "../../components/Forecast/Forecast";
import Error from "../../components/Error";
import Loader from "../../components/Loader";
import { SelectBox } from "devextreme-react/select-box";

export default () => {
    const {
        isError,
        isLoading,
        forecast,
        countryList,
        submitRequest,
        upcomingForecasts,
    } = useForecast();
    const [country, setCountry] = useState(null);

    /**
     * @param component selected item of search box
     * @description event handler of search box
     */
    const onCountryChange = ({ component }: any) => {
        const country = component._getCurrentValue();
        setCountry(country);
        submitRequest(country);
    };

    return (
        <Fragment>
            <Header />
            <div className={styles.searchBox}>
                <SelectBox
                    dataSource={countryList}
                    displayExpr="name"
                    searchEnabled={true}
                    searchMode={"contains"}
                    searchExpr={"name"}
                    searchTimeout={200}
                    placeholder={"Select country"}
                    minSearchLength={0}
                    value={
                        country == null && countryList.length > 0
                            ? countryList[0]
                            : country
                    }
                    className={styles.searchEdit}
                    showDataBeforeSearch={false}
                    onValueChanged={onCountryChange}
                />
            </div>
            {(isLoading || !forecast) && (
                <div className={`${styles.box} position-relative`}>
                    {isError && <Error message="Error occured" />}
                    {isLoading && <Loader />}
                </div>
            )}
            {!isLoading && forecast && (
                <Forecast
                    onSubmit={submitRequest}
                    forecast={forecast}
                    upcomingForecasts={upcomingForecasts}
                />
            )}
        </Fragment>
    );
};
